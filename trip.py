"""Record travels
Created by Tiria Andersen, October 2015"""


class Error(Exception):
    """To be used when error-checking in details"""
    def __init__(self, value):
        super().__init__(self)


class Country:
    def __init__(self, country_name, currency_code, currency_symbol):
        """Initialise country instance"""
        self.country_name = country_name
        self.currency_code = currency_code
        self.currency_symbol = currency_symbol

    def format_currency(self, money_amount):
        """Adds currency symbol in from of amount, and rounds to the nearest 2nd decimal"""
        money_amount = self.currency_symbol + format(money_amount, ".2f")
        return money_amount

    def __str__(self):
        """Gives country details as a string separated by spaces"""
        return "{} {} {}".format(self.country_name, self.currency_code, self.currency_symbol)


class Details:
    def __init__(self):
        """Defines location field as list"""
        self.locations = []

    def add(self, country_name, start_date, end_date):
        """Method to add information of visited countries at certain times, into a list."""
        # creates list of year, month and day, assuming input is in YYYY/MM/DD format
        start_date_list = tuple(start_date.split("/"))
        end_date_list = tuple(end_date.split("/"))
        dates = (start_date_list, end_date_list)
        for date in dates:
            # checks if year, date and month have been inputted, by expected length of each component, raises error if incorrect
            if not (len(date[0]) == 4 and len(date[1]) == 2 and len(date[2]) == 2):
                raise Error("Not valid date")
            else:
                # checks if each part of the date is a number, raises error if incorrect
                for parts in date:
                    if not parts.isdigit():
                        raise Error("Not a digit")
        for item in self.locations:
            # checks if start date is in an entry's time frame, raises error if it is.
            if start_date == item[1]:
                raise Error("Already gone to a location on this start date.")
        if start_date > end_date:
            # raises error if start date is after end date
            raise Error("The start date is after the end date.")
        # information considered valid and thus added to list
        details = (country_name, start_date, end_date)
        self.locations.append(details)

    def current_country(self, date_string):
        """Finds location at specified time by searching location list."""
        for trip in self.locations:
            if trip[1] < date_string < trip[2]:
                return trip[0]

    def is_empty(self):
        """Determines if list is empty or not."""
        return self.locations == []


# CODING TESTING
def main():
    try:
        australia = Country("Australia", "AUD", "$")
        print(australia.format_currency(10.556))  # are the decimals are rounded two decimal places?
        print(australia.__str__())  # In the right format?
    except:
        "Country class failed"
    holiday = Details()
    try:
        holiday.add("Japan", "1900/09/40", "1990/06/20")
    except:
        print("add function failed")
    try:
        holiday.add("Australia", "2000/09/40", "2005/04/20")
    except:
        print("Add function failed")
    print(holiday.locations)  # Check if add function has stored info properly
    try:  # Induce "not valid date" error by changing length of start date.
        holiday.add("Japan", "19001/09/40", "1990/06/20")
    except:
        print("not valid date error failed")
    try:  # Induce "not valid date" error by changing length of end date.
        holiday.add("Japan", "1900/09/40", "1990/061/20")
    except:
        print("not valid date error")
    try:  # Induce "not a digit" error by putting letter in end date.
        holiday.add("Japan", "1900/09/40", "19q0/061/20")
    except:
        print("not a digit error")
    try:  # Induce "not a digit" error by putting letter in start date
        holiday.add("Japan", "1900/0a/40", "1990/061/20")
    except:
        print("not a digit error")
    try:  # Induce "start date is after end date" error.
        holiday.add("Japan", "2000/09/40", "1990/061/20")
    except:
        print("start date after end date error")
    try:  # Induce "already gone to a location on this start date" error
        holiday.add("Australia", "2000/09/40", "2005/04/20")
    except:
        print("already in list error")
    try:
        # Check if identical end date induces "already gone to a location on this start date" error
        holiday.add("Australia", "2000/04/20", "2000/09/40")
    except:
        print("add function failed")
    print(holiday.current_country("1994/12/30"))  # Should return none
    print(holiday.current_country("1901/12/30"))  # Should return location
    print(holiday.is_empty())  # Should return False


if __name__ == '__main__':
    main()
