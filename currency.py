"""Convert currencies from one country to another

created by Tiria Andersen, October 2015"""


import web_utility


def convert(amount, home_currency_code, location_currency_code):
    """ Returns another equivalent currency  specified amount of money """
    try:
        url_string = "https://www.google.com/finance/converter?a={}&from={}&to={}".format(amount, home_currency_code,
                                                                                          location_currency_code)
        result = web_utility.load_page(url_string)  # accesses and reads web page
        extracted_string = (result[result.index('result'):])
        listed_string = extracted_string.split(">")
        converted_result = listed_string[2].split(" ")
        return float(converted_result[0])
    except:
        return -1


def get_details(country_name):
    part = []
    input_file = open("currency_details.txt", mode="r", encoding="utf-8")
    for line in input_file:
        line = line.strip("\n").split(",")
        if country_name in line:
            part.append(line)
    input_file.close()
    return tuple(part)


# CODING TESTING

def formatting_test_code(conversion, amount, home_currency_code, location_currency_code):
    """To assist with numerous iterations of testing code"""
    converted_amount = format(convert(amount, home_currency_code, location_currency_code), ".2f")
    return "{} {} {}->{} {} ".format(conversion, amount, home_currency_code, location_currency_code, converted_amount)


def main():
    try:  # testing convert function
        print(formatting_test_code("invalid conversion", 1, "AUD", "AUD"))
        print(formatting_test_code("invalid conversion", 1, "JPY", "ABC"))
        print(formatting_test_code("invalid conversion", 1, "ABC", "USD"))
        print(formatting_test_code("valid conversion", 10.95, "AUD", "JPY"))
        print(formatting_test_code("valid conversion reverse", 943.18, "JPY", "AUD"))
        print(formatting_test_code("valid conversion", 10.95, "AUD", "JPY"))
        print(formatting_test_code("valid conversion reverse", 13.62, "BGN", "AUD"))
        print(formatting_test_code("valid conversion", 200.15, "BGN", "JPY"))
        print(formatting_test_code("valid conversion reverse", 13859.49, "JPY", "BGN"))
        print(formatting_test_code("valid conversion", 100.00, "JPY", "BGN"))
        print(formatting_test_code("valid conversion reverse", 0.83, "USD", "JPY"))
        print(formatting_test_code("valid conversion", 19.99, "USD", "BGN"))
        print(formatting_test_code("valid conversion reverse", 34.58, "BGN", "USD"))
        print(formatting_test_code("valid conversion", 19.99, "USD", "AUD"))
        print(formatting_test_code("valid conversion reverse", 27.80, "AUD", "USD"))
    except:
        print("Convert function failed.")
    try:  # testing get_details function
        print("invalid details" + str(get_details("Unknown")))
        print("invalid details" + str(get_details("Japanese")))
        print("invalid details" + str(get_details(" ")))
        print("valid details" + str(get_details("Australia")))
        print("valid details" + str(get_details("Japan")))
        print("valid details" + str(get_details("Hong Kong")))
    except:
        print("get_details function failed.")


if __name__ == '__main__':
    main()

